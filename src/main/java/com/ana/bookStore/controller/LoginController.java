package com.ana.bookStore.controller;


import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.model.User;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.CryptingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;

@Controller
public class LoginController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserBookRepository userBookRepository;
    @Autowired
    LoginHolder loginHolder;
    @Autowired
    private CryptingService cryptingService;

    @RequestMapping(value="/login", method= RequestMethod.GET )
    public String login(@RequestParam("email") String email, @RequestParam("password") String password) throws Exception {
        User user = userRepository.findByEmailAddress(email);
        try {
            if(user != null) {
                if (!cryptingService.matches(user.getPassword(), password))
                    throw new Exception("Username or password is incorrect. Please try again!");
            }
            else
                throw new Exception("Username or password is incorrect. Please try again!");

            loginHolder.setCurrentUserId(user.getId());
        } catch (NoSuchAlgorithmException e){
            throw new Exception("Password doesn't match with " + user.getPassword() + ".");
        }

        return "redirect:/";
    }

    @RequestMapping(value="/logout", method= RequestMethod.GET )
    public String logout() {
        loginHolder.setCurrentUserId(0L);
        return "redirect:/";
    }
}
