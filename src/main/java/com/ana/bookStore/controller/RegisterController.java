package com.ana.bookStore.controller;

import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.model.User;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.CryptingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpSession;
import java.util.Random;

@Controller
public class RegisterController {

	public static String sessionId;
	@Autowired
	UserRepository userRepository;
	@Autowired
	LoginHolder loginHolder;
	@Autowired
	private CryptingService cryptingService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String saveUser(@RequestParam("emailAddress") String email,
						   @RequestParam("password") String password,
						   @RequestParam("confirmPassword") String confirmPassword,
						   @RequestParam("address") String address,
						   final HttpSession session) throws Exception {
		sessionId = session.getId();
		if (email == null || password == null || confirmPassword == null || address == null) {
			throw new Exception("All fields are required!");
		}
		if (!password.equals(confirmPassword) || userRepository.findByEmailAddress(email) != null) {
			throw new Exception("Passwords don't match!");
		}

		if (userRepository.findByEmailAddress(email) != null) {
			throw new Exception("User with address " + email + " already exists!");
		}

		User user = new User(email.substring(0, email.indexOf("@")), email, cryptingService.encrypt(password), address);
		userRepository.save(user);
		User currentUser = userRepository.findByEmailAddress(email);
		loginHolder.setCurrentUserId(currentUser.getId());
		return "redirect:/";
	}
}



