package com.ana.bookStore.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
//@Proxy(lazy = false)
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String bookTitle;

    private String author;

    private String category;

    @Lob
    @Column(name = "description", length = 5000)
    private String description;

    private String photo;

    private double price;

    private int quantity;
    @OneToMany(mappedBy = "book")
    private List<UserBook> users = new ArrayList<>();
    @OneToMany(mappedBy = "book",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Review> reviews = new ArrayList<>();

    public Book() {}

    public Book(String bookTitle){
        this.bookTitle = bookTitle;
    }

    public Book(Long id, String bookTitle){
        this.id = id;
        this.bookTitle = bookTitle;
    }


    public Book(String bookTitle, String category){
        this.bookTitle = bookTitle;
        this.category = category;
    }


    public Book(String bookTitle, String author, String description, String category, double price, int quantity){
        this.bookTitle = bookTitle;
        this.author = author;
        this.description = description;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<UserBook> getUsers() {
        return users;
    }

    public void setUsers(List<UserBook> users) {
        this.users = users;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public void addReview(Review review){
        reviews.add(review);
        review.setBook(this);
    }
    public void removeReview(Review review){
        reviews.remove(review);
        review.setBook(null);
    }
}
