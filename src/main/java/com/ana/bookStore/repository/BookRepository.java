package com.ana.bookStore.repository;

import com.ana.bookStore.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Repository
public interface BookRepository extends JpaRepository<Book, Long> {


    public Optional<Book> findById(Long id);

    public Book findByBookTitle(String title);

    public List<Book> findAllByCategory(String category);

    public List<Book> findByBookTitleIgnoreCase(String bookTitle);
    @Transactional
    public void deleteById(Long id);
    @Transactional
    public void deleteAllByCategory(String category);

}
