package com.ana.bookStore.repository;

import com.ana.bookStore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User save(User user);

    User findByUsername(String username);

    User findByEmailAddress(String emailAddress);

    Optional<User> findById(Long id);



}

