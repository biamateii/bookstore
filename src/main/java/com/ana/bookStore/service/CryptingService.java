package com.ana.bookStore.service;

import org.bouncycastle.util.encoders.Hex;
import org.springframework.stereotype.Service;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.management.openmbean.InvalidKeyException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;

@Service
public class CryptingService {


	public String encrypt(String password) throws NoSuchAlgorithmException {

		if (password.isEmpty())
			throw new NoSuchAlgorithmException("Empty password");
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(
				password.getBytes(StandardCharsets.UTF_8));
		String sha256hex = new String(Hex.encode(hash));
		return "SHA-256" + sha256hex + "SHA-256";
	}

	public boolean matches(String hashed, String password) throws NoSuchAlgorithmException {
		return encrypt(password).equals(hashed);

	}

}
