package com.ana.bookStore.service;

import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.model.User;
import com.ana.bookStore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    private LoginHolder loginHolder;

    public Optional<User> findById(Long id){
       return  userRepository.findById(id);
    }

    public boolean isAdmin() {
        User user = findById(loginHolder.getCurrentUserId()).orElse(null);
        return user != null && user.getEmailAddress().equals("admin@booktique.com");
    }
}
