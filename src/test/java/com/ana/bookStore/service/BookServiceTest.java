package com.ana.bookStore.service;

import com.ana.bookStore.BookRating;
import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.Review;
import com.ana.bookStore.model.User;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.ReviewRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class BookServiceTest {

    @Mock
    private BookRepository bookRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ReviewRepository reviewRepository;
    @Mock
    private LoginHolder loginHolder;
    @InjectMocks
    private BookService bookService;

    @Test
    public void whenBookReviewFromSameUserIsPresentThenItShouldBeUpdatedWithCorrespondingRating() throws Exception {
        Optional<Book> book = Optional.of(new Book());
        Optional<Review> review = Optional.of(new Review());
        review.get().setText("Review");
        when(bookRepository.findById(any(Long.class))).thenReturn(book);
        when(reviewRepository.findByUserIdAndBookId(any(Long.class), any(Long.class))).thenReturn(review);
        when(reviewRepository.save(any(Review.class))).thenReturn(review.get());
        bookService.addBookRating("Author Name", 1L, 1L, "book rating");
        assertThat(review.get().getText()).isNotNull();

    }

    @Test
    public void calculateRatingsPerBook() throws Exception {
        Review firstReview  = new Review();
        Review secondReview = new Review();

        firstReview.setBookRating(BookRating.MEDIOCRE);
        secondReview.setBookRating(BookRating.EXCELLENT);

        List<Review> reviews = Arrays.asList(firstReview, secondReview);
        when(reviewRepository.findAllByBookId(any(Long.class))
                .stream()
                .filter(review -> review.getBookRating() != null)
                .collect(Collectors.toList())).thenReturn(reviews);
        Map<String, String> ratings = bookService.calculateRatingsPerBook(any(Long.class));
        assertThat(ratings.get("Total Ratings")).isEqualTo("6");
        assertThat(ratings.get("Excellent Rating")).isEqualTo("66%");
        assertThat(ratings.get("Good Rating")).isEqualTo("0%");
        assertThat(ratings.get("Mediocre Rating")).isEqualTo("33%");
        assertThat(ratings.get("Poor Rating")).isEqualTo("0%");

    }
}
